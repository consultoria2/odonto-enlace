import { createRouter, createWebHashHistory } from '@ionic/vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Welcome from '../views/Welcome.vue';
import PreRegister from '../views/PreRegister.vue';
import Register from '../views/Register.vue';
import EditProfile from '../views/EditProfile.vue';
import Marketplace from '../views/marketplace/Home.vue';
import EditStore from '../views/marketplace/EditStore.vue';
import ListProducts from '../views/marketplace/ListProducts.vue';
import AddProduct from '../views/marketplace/AddProduct.vue';
import Cart from '../views/marketplace/Cart.vue';
import Favorites from '../views/marketplace/Favorites.vue';
import Similars from '../views/marketplace/Similars.vue';
import Offers from '../views/marketplace/Offers.vue';
import Checkout from '../views/marketplace/Checkout.vue';
import Orders from '../views/marketplace/Orders.vue';
import OrderDetails from '../views/marketplace/OrderDetails.vue';
import StoreOrders from '../views/marketplace/StoreOrders.vue';
import Search from '../views/marketplace/Search.vue';
import StoreProfile from '../views/marketplace/StoreProfile.vue';
import ProductDetail from '../views/marketplace/ProductDetail.vue';
import Stores from '../views/marketplace/Stores.vue';
import Questions from '../views/marketplace/Questions.vue';
import Schedule from '../views/assistant/Schedule.vue';
import Patients from '../views/assistant/Patients.vue';
import Assistant from '../views/assistant/Home.vue';
import Chat from '../views/chat/Chat.vue';
import MyChats from '../views/chat/MyChats.vue';

/* Mixins */
import api from "../api";
import store from "../store";

import { Storage } from "@ionic/storage";


const storage = new Storage();

const routes = [
  {
    path: '/',
    redirect: '/welcome',
    name: 'Root'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    beforeEnter: (to, from) => {
      console.log('Watching Home (from, to)...');
      console.log(from);
      console.log(to);
      if (from && from.path && from.path.indexOf('home') > 0) {
        return false;
      }
    },
  },
  {
    path: '/welcome',
    name: 'Welcome',
    component: Welcome
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    beforeEnter: (to, from) => {
      console.log('Watching Login (from, to)...');
      console.log(from);
      console.log(to);
      if (from && from.path && from.path.indexOf('home') > 0) {
        return false;
      }
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/pre-register',
    name: 'PreRegister',
    component: PreRegister
  },
  {
    path: '/profile/edit',
    name: 'Editar Perfil',
    component: EditProfile
  },
  {
    path: '/profile/edit/offices',
    name: 'Editar Oficinas del Perfil',
    component: EditProfile
  },
  {
    path: '/marketplace/home',
    name: 'Marketplace',
    component: Marketplace
  },
  {
    path: '/marketplace/store/edit',
    name: 'Mi Tienda',
    component: EditStore
  },
  {
    path: '/marketplace/store/products',
    name: 'Mis Productos',
    component: ListProducts
  },
  {
    path: '/marketplace/store/add/product',
    name: 'Crear Producto',
    component: AddProduct
  },
  {
    path: '/marketplace/store/edit/product/:id',
    name: 'Editar Producto',
    component: AddProduct
  },
  {
    path: '/marketplace/cart',
    name: 'Carrito',
    component: Cart
  },
  {
    path: '/marketplace/favorites',
    name: 'Favoritos',
    component: Favorites
  },
  {
    path: '/marketplace/similars/:id',
    name: 'Similares',
    component: Similars
  },
  {
    path: '/marketplace/offers',
    name: 'Ofertas',
    component: Offers
  },
  {
    path: '/marketplace/store/offers/:store',
    name: 'Ofertas de Tienda',
    component: Offers
  },
  {
    path: '/marketplace/checkout',
    name: 'Checkout',
    component: Checkout
  },
  {
    path: '/marketplace/store/orders',
    name: 'Pedidos Recibidos',
    component: StoreOrders
  },
  {
    path: '/marketplace/orders',
    name: 'Mis Compras',
    component: Orders
  },
  {
    path: '/marketplace/store/orders/details/:id',
    name: 'Detalle de Pedido',
    component: OrderDetails
  },
  {
    path: '/marketplace/orders/details/:id',
    name: 'Detalle de Compra',
    component: OrderDetails
  },
  {
    path: '/marketplace/search/:query',
    name: 'Buscar',
    component: Search
  },
  {
    path: '/marketplace/search/:query/full-search',
    name: 'Buscar Completo',
    component: Search
  },
  {
    path: '/marketplace/starred/search/:query',
    name: 'Buscar en Destacados',
    component: Search
  },
  {
    path: '/marketplace/category/search/:query',
    name: 'Buscar en Categoria',
    component: Search
  },
  {
    path: '/marketplace/search/:store/:query/full-search',
    name: 'Buscar en Tienda',
    component: Search
  },
  {
    path: '/marketplace/catalog/search/:store',
    name: 'Catalogo de Tienda',
    component: Search
  },
  {
    path: '/marketplace/catalog/search/:store/:query',
    name: 'Buscar en Catalogo de Tienda',
    component: Search
  },
  {
    path: '/marketplace/catalog/search/:store/:query/full-search',
    name: 'Buscar en Catalogo de Tienda',
    component: Search
  },
  {
    path: '/marketplace/catalog/search/:store/:query/:category',
    name: 'Buscar en Catalogo de Tienda con Categoria Fijada',
    component: Search
  },
  {
    path: '/marketplace/catalog/search/:store/:query/:category/full-search',
    name: 'Buscar en Catalogo de Tienda con Categoria Fijada sin Limite',
    component: Search
  },
  {
    path: '/marketplace/profile/:store',
    name: 'Tienda',
    component: StoreProfile
  },
  {
    path: '/marketplace/stores',
    name: 'Tiendas',
    component: Stores
  },
  {
    path: '/marketplace/product/:id',
    name: 'Detalle de Producto',
    component: ProductDetail
  },
  {
    path: '/marketplace/questions',
    name: 'Preguntas',
    component: Questions
  },
  {
    path: '/assistant/patients',
    name: 'Pacientes',
    component: Patients
  },
  {
    path: '/assistant/patients/:id',
    name: 'Paciente',
    component: Patients
  },
  {
    path: '/assistant/schedule',
    name: 'Agenda',
    component: Schedule
  },
  {
    path: '/assistant/home',
    name: 'Assistant',
    component: Assistant
  },
  {
    path: '/chat/:id',
    name: 'Mensajeria',
    component: Chat
  },
  {
    path: '/mychats',
    name: 'Mis Mensajes',
    component: MyChats
  },
]

const router = createRouter({
  history: createWebHashHistory('app'),
  routes,
  scrollBehavior() {
    document.getElementById('app').scrollIntoView();
  }
})

router.beforeEach(async (to, from) => {
  console.info('Navigation Guard...');
  console.info('To:', to, 'From:', from);

  var isAuth = false;

  await storage.create();

  var user = await storage.get("user");
  var accessToken = await storage.get("accessToken");

  if(['Login', 'Register', 'PreRegister', 'Marketplace', 'Detalle de Producto', 'Tienda', 
      'Buscar en Tienda', 'Buscar', 'Buscar en Categoria', 'Buscar Completo', 'Buscar en Destacados', 
      'Catalogo de Tienda', 'Ofertas de Tienda', 'Buscar en Catalogo de Tienda con Categoria Fijada', 'Similares', 'Ofertas',
      'Buscar en Catalogo de Tienda con Categoria Fijada sin Limite'].includes(to.name)) {
    return true;
  }

  if (user === null) {
    console.log("User is not set...");

    if (accessToken === null) {

      // Clearing storage and logging out
      console.log("Access token is not set. Logging out...");
      storage.clear();

      if (to.name == 'Welcome'){
        console.log('Redirecting to Welcome');
        return true;
      } else {
        return { name: 'Welcome' };
      }
      

    } else {
      // Access token exists
      console.log("Attemping sign in with access token...");
      store.commit("setAccessToken", accessToken);

      // Retrieve user
      await api
        .get("me", {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        })
        .then(async (response) => {
          console.log(response);

          let parsedValue = JSON.parse(JSON.stringify(response.data));
          storage.set("user", parsedValue);

          store.commit("setUser", parsedValue);
          console.log(`¡Bienvenido, ${response.data.name}!`);
        })
        .catch(function (error) {
          console.log(error);
          console.error(JSON.stringify(error.response.data));
        });
    }
  } else {
    console.log("User is set...");
    store.commit("setUser", user);
    
    if (accessToken === null) {

      // Clearing storage and logging out
      console.log("Access token is not set. Logging out...");
      storage.clear();
      return { name: 'Welcome' };

    } else {

      // Access token exists
      console.log(`Commiting existing access token (${accessToken})...`);
      store.commit("setAccessToken", accessToken);

      if (to.name == 'Welcome'){
        console.log('Redirecting to Assistant');
        return { name: 'Assistant' };
      } else {
        return true;
      }
      
    }
  }

  return isAuth;
})

// const checkAuth = async () => {

//   // Verify if user exists
//   storage.get("user").then(async (result) => {
//     if (result === null) {
//       console.log("User is not set...");

//       // Verify if token exists
//       storage.get("accessToken").then(async (result) => {
//         if (result === null) {
//           // Clearing storage and logging out
//           console.log("Access token is not set. Logging out...");
//           this.clearLocalStorage();
//           this.$router.push("/");
//         } else {
//           // Access token exists
//           console.log("Attemping sign in with access token...");
//           this.$store.commit("setAccessToken", result);

//           // Retrieve user
//           api
//             .get("me", {
//               headers: {
//                 Authorization: `Bearer ${result}`,
//               },
//             })
//             .then((response) => {
//               console.log(response);

//               let parsedValue = JSON.parse(JSON.stringify(response.data));
//               this.setLocalStorage("user", parsedValue);
//               this.$store.commit("setUser", parsedValue);
//               this.$router.push("/assistant/home");
//               console.log(`¡Bienvenido, ${response.data.name}!`);
//             })
//             .catch(function (error) {
//               console.log(error);
//               console.error(JSON.stringify(error.response.data));
//             });
//         }
//       });
//     } else {
//       console.log('User is set');

//       store.commit("setUser", result);

//       // Force access token update on vuex
//       storage.get("accessToken").then(async (result) => {
//         if (result === null) {
//           // Clearing storage and logging out
//           console.log("Access token is not set. Logging out...");
//           this.clearLocalStorage();
//           this.$router.push("/");
//         } else {

//           // Access token exists
//           console.log(`Commiting existing access token (${result})...`);
//           store.commit("setAccessToken", result);

//           return true;
//         }
//       });
//     }
//   });
// }

export default router
