import axios from 'axios';

const axiosInstance = axios.create({
    // baseURL: 'http://localhost:8000/api/'
    // baseURL: 'https://api.odontoenlace.com/api/'
    baseURL: 'https://odontoenlace.com/api/'
})

export default axiosInstance
