import { createI18n } from 'vue-i18n'

const messages = {
    en: {
      message: {
        patient: 'Patient',
        dentist: 'Dentist',
        student: 'Student'
      }
    },
    es: {
        message: {
          patient: 'Paciente',
          dentist: 'Odontologo',
          student: 'Estudiante'
        }
      },
    ja: {
      message: {
        hello: 'こんにちは、世界'
      }
    }
  }

  const i18n = createI18n({
    locale: 'es', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
    // If you need to specify other options, you can set other options
    // ...
  })

export default i18n