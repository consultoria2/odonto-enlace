const idType = [
    {   
        "name":"Venezolano",
        "abbreviation":"V",
    },
    {
        "name":"Extranjero",
        "abbreviation":"E",
    },
    {
        "name":"Jurídico",
        "abbreviation":"J",
    },
    {
        "name":"Gubernamental",
        "abbreviation":"G",
    }
];

export default idType