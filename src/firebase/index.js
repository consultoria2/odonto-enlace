// Import the functions you need from the SDKs you need
import firebase from "firebase";
import "firebase/firestore";
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDv3NpC_qz8PPgkUqruadu6WvZUDO3qCoE",
  authDomain: "odontoenlace-4ff1f.firebaseapp.com",
  projectId: "odontoenlace-4ff1f",
  storageBucket: "odontoenlace-4ff1f.appspot.com",
  messagingSenderId: "831420659766",
  appId: "1:831420659766:web:5ab47de97709a4705b4097",
  measurementId: "G-5TPY7D7TZH"
};

// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);
export default fire
