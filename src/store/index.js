import { createStore } from 'vuex'
import api from "../api";

// Create a new store instance.
const store = createStore({
    state() {
        return {
            count: 0,
            user: {},
            access_token: null,
            cart: [],
            currency: 'USD',
            categories: [],
            universities: [],
            validShipping: false,
            validInvoicing: false,
            validPayment: false,
            validMultiPayment: false,
            store: {},
            currentModule: 'Mercado',
            numNotRead: 0,
            currentPatient: {}
        }
    },
    mutations: {
        increment(state) {
            state.count++
        },
        setNumNotread(state, payload) {
            state.numNotRead = payload;
        },
        setUser(state, payload) {
            state.user = payload;
        },
        setCurrentPatient(state, payload) {
            state.currentPatient = payload;
        },
        setAccessToken(state, payload) {
            state.access_token = payload;
        },
        addToCart(state, payload) {
            const found = state.cart.find(element => element.id == payload.id);

            if (!found){
                state.cart.push(payload);

                console.log('Added to cart: ' + JSON.stringify(payload));
                console.log('Cart size: ' + state.cart.length);
            } else {
                if(found.qty < found.stock){
                    found.qty++;
                }
                console.log('Product already exist in cart: ' + JSON.stringify(payload));
            }
            
        },
        emptyCart(state) {
            state.cart = [];
        },
        setCurrency(state, payload) {
            state.currency = payload;
        },
        setValidShipping(state, payload) {
            state.validShipping = payload;
        },
        setValidInvoicing(state, payload) {
            state.validInvoicing = payload;
        },
        setValidPayment(state, payload) {
            state.validPayment = payload;
        },
        setValidMultiPayment(state, payload) {
            state.validMultiPayment = payload;
        },
        setStore(state, payload) {
            state.store = payload;
        },
        setChatReceiver(state, payload) {
            state.chatReceiver = payload;
        },
        async getCategoriesHandler(state) {
            await api
            .get("categories/list")
            .then(response => {

                var categories = response.data;

                state.categories = [];

                categories.forEach(element => {
                    state.categories.push({
                        value: element.id,
                        label: element.category_name
                    })
                });
                
                console.log('Available Categories:');
                console.log(state.categories);
            })
            .catch(error => {
                console.log(error);
            });
        },
        async getUniversitiesHandler(state) {
            await api
            .get("institutes/list/universidad")
            .then(response => {

                var universities = response.data;

                state.universities = [];

                universities.forEach(element => {
                    state.universities.push({
                        value: element.id,
                        label: element.institute_name
                    })
                });
                
                console.log('Available Universities:');
                console.log(state.universities);
            })
            .catch(error => {
                console.log(error);
            });
        },
    },
    getters: {
        getUser(state) {
            return state.user;
        },
        getCurrentPatient(state) {
            return state.currentPatient;
        },
         getNumNotRead(state) {
            return state.numNotRead;
        },
        getStoreId(state) {
            return state.user && state.user.stores ? state.user.stores[0].id : null;
        },
        getAccessToken(state) {
            return state.access_token;
        },
        getCart(state){
            return state.cart;
        },
        getCurrency(state){
            return state.currency;
        },
        getCategories(state){
            return state.categories;
        },
        getUniversities(state){
            return state.universities;
        },
        getValidShipping(state){
            return state.validShipping;
        },
        getValidInvoicing(state){
            return state.validInvoicing;
        },
        getValidPayment(state){
            return state.validPayment;
        },
        getValidMultiPayment(state){
            return state.validMultiPayment;
        },
        getStore(state){
            return state.store;
        },
        getCurrentModule(state){
            return state.currentModule;
        },
        getChatReceiver(state){
            return state.chatReceiver;
        }
    },
    actions: {
        getCategoriesOptions(context, data) {
            context.commit('getCategoriesHandler', {data, context})
        },
        getUniversitiesOptions(context, data) {
            context.commit('getUniversitiesHandler', {data, context})
        },
    }
})

export default store
