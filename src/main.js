import { createApp } from 'vue'
import MainApp from './MainApp.vue'
import router from './router';

import { IonicVue, getPlatforms } from '@ionic/vue';

import { App } from '@capacitor/app';

/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

/* Theme variables */
import './theme/variables.css';

/* Addition plugins */
import VueFinalModal from 'vue-final-modal';
import i18n from './i18n';
// import PortalVue from "portal-vue";

/* Own components*/
import ProductCard from './components/ProductCard.vue';
import MoreCard from './components/MoreCard.vue';
import HiddenCard from './components/HiddenCard.vue';
import ImageCard from './components/ImageCard.vue';
import StoreCard from './components/StoreCard.vue';
import PatientCard from './components/PatientCard.vue';
import ProductLine from './components/ProductLine.vue';
import OrderLine from './components/OrderLine.vue';
import Accordion from './components/Accordion.vue';
import MenuAccordion from './components/MenuAccordion.vue';
import EditProfileAccordion from './components/EditProfileAccordion.vue';
import MenuPopover from './components/MenuPopover.vue';
import Message from './components/Message.vue';
import Multiselect from '@vueform/multiselect';
import Loading from 'vue-loading-overlay';
import dayjs from 'dayjs';
import 'vue-loading-overlay/dist/vue-loading.css';

import OneSignal from 'onesignal-cordova-plugin';

/* More components */

import { SetupCalendar, Calendar, DatePicker } from 'v-calendar';

/* Mixins */
import StorageService from './storage'

/* Vuex Store */
import store from './store'

const app = createApp(MainApp)
  .use(IonicVue)
  .use(router)
  .use(VueFinalModal())
  .use(i18n)
  .use(store)
  .use(SetupCalendar, {})
  .mixin(StorageService);

// Declaring globally the product card component
app.component('product-card', ProductCard);
app.component('more-card', MoreCard);
app.component('hidden-card', HiddenCard);
app.component('image-card', ImageCard);
app.component('store-card', StoreCard);
app.component('patient-card', PatientCard);
app.component('product-line', ProductLine);
app.component('order-line', OrderLine);
app.component('accordion', Accordion);
app.component('menu-accordion', MenuAccordion);
app.component('edit-profile-accordion', EditProfileAccordion);
app.component('menu-popover', MenuPopover);
app.component('multiselect', Multiselect);
app.component('loading', Loading);
app.component('message', Message);
app.component('Calendar', Calendar);
app.component('DatePicker', DatePicker);
app.config.globalProperties.$dayjs = dayjs;

router.isReady().then(() => {
  app.mount('#app');

  App.addListener('appUrlOpen', function (event) {
    // Example url: https://beerswift.app/tabs/tabs2
    // slug = /tabs/tabs2
    const slug = event.url.split('.com/app#').pop();
  
    // We only push to the route if there is a slug present
    if (slug) {
      router.push({
        path: slug,
      });
    }
    console.log(slug);
  });
});

// Configuring OneSignal notifications

// Call this function when your app start
function OneSignalInit() {
  // Uncomment to set OneSignal device logging to VERBOSE  
  OneSignal.setLogLevel(6, 0);

  // NOTE: Update the setAppId value below with your OneSignal AppId.
  OneSignal.setAppId("0621d600-f4ef-4c27-a2cd-08a335aeea4b");
  OneSignal.setNotificationOpenedHandler(function(jsonData) {
      console.log('notificationOpenedCallback (' + window.location.href + '): ' + JSON.stringify(jsonData) );
  });

  // iOS - Prompts the user for notification permissions.
  //    * Since this shows a generic native prompt, we recommend instead using an In-App Message to prompt for notification permission (See step 6) to better communicate to your users what notifications they will get.
  OneSignal.promptForPushNotificationsWithUserResponse(function(accepted) {
      console.log("User accepted notifications: " + accepted);
  });
}

if (getPlatforms().includes("capacitor")) {
  // document.addEventListener('deviceready', OneSignalInit, false);
  OneSignalInit();
} else {
  window.OneSignal = window.OneSignal || [];
  window.OneSignal.push(function () {

    window.OneSignal.SERVICE_WORKER_PARAM = { scope: '/assets/js/onesignal/' };
    window.OneSignal.SERVICE_WORKER_PATH = 'assets/js/onesignal/OneSignalSDKWorker.js'
    window.OneSignal.SERVICE_WORKER_UPDATER_PATH = 'assets/js/onesignal/OneSignalSDKUpdaterWorker.js'

    window.OneSignal.init({
      appId: "0621d600-f4ef-4c27-a2cd-08a335aeea4b",
      safari_web_id: "web.onesignal.auto.0621d600-f4ef-4c27-a2cd-08a335aeea4b",
      notifyButton: {
        enable: false,
      },
      subdomainName: "odontoenlace",
      promptOptions: {
        slidedown: {
          prompts: [
            {
              type: "push", // current types are "push" & "category"
              autoPrompt: true,
              text: {
                /* limited to 90 characters */
                actionMessage: "Nos gustaría enviarte notificaciones para que estés al tanto de eventos en tu perfil.",
                /* acceptButton limited to 15 characters */
                acceptButton: "Aceptar",
                /* cancelButton limited to 15 characters */
                cancelButton: "Cancelar"
              },
              delay: {
                pageViews: 0,
                timeDelay: 1
              }
            }
          ]
        }
      }
    });
  });
}