"use strict";(self["webpackChunkodonto_enlace"]=self["webpackChunkodonto_enlace"]||[]).push([[990],{8990:function(e,t,n){n.r(t),n.d(t,{createSwipeBackGesture:function(){return a}});var o=n(6587),r=n(545),c=n(6515);
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const a=(e,t,n,a,i)=>{const s=e.ownerDocument.defaultView,u=(0,r.i)(e),l=e=>{const t=50,{startX:n}=e;return u?n>=s.innerWidth-t:n<=t},d=e=>u?-e.deltaX:e.deltaX,h=e=>u?-e.velocityX:e.velocityX,f=e=>l(e)&&t(),k=e=>{const t=d(e),n=t/s.innerWidth;a(n)},w=e=>{const t=d(e),n=s.innerWidth,r=t/n,c=h(e),a=n/2,u=c>=0&&(c>.2||t>a),l=u?1-r:r,f=l*n;let k=0;if(f>5){const e=f/Math.abs(c);k=Math.min(e,540)}i(u,r<=0?.01:(0,o.j)(0,r,.9999),k)};return(0,c.createGesture)({el:e,gestureName:"goback-swipe",gesturePriority:40,threshold:10,canStart:f,onStart:n,onMove:k,onEnd:w})}}}]);
//# sourceMappingURL=990.f156c798.js.map