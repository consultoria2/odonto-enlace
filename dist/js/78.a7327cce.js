"use strict";(self["webpackChunkodonto_enlace"]=self["webpackChunkodonto_enlace"]||[]).push([[78],{6078:function(e,t,n){n.r(t),n.d(t,{startStatusTap:function(){return a}});var o=n(65),r=n(8487),s=n(6587);
/*!
 * (C) Ionic http://ionicframework.com - MIT License
 */
const a=()=>{const e=window;e.addEventListener("statusTap",(()=>{(0,o.wj)((()=>{const t=e.innerWidth,n=e.innerHeight,a=document.elementFromPoint(t/2,n/2);if(!a)return;const c=(0,r.a)(a);c&&new Promise((e=>(0,s.c)(c,e))).then((()=>{(0,o.Iu)((async()=>{c.style.setProperty("--overflow","hidden"),await(0,r.s)(c,300),c.style.removeProperty("--overflow")}))}))}))}))}}}]);
//# sourceMappingURL=78.a7327cce.js.map